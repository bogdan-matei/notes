
# RTX workstations

## rtx9

- IP address: 10.5.4.51
- MAC address: b0:7b:25:18:80:64
- User: rtx-9
- Password: Leddartech123

----------
----------

# Nvidia Xavier

## NRU1

- IP address: 10.5.4.54
- MAC address: 78:d0:04:2d:82:49
- User: nvidia
- Password: nvidia
- Mount (fstab): `sshfs#nvidia@10.5.4.54:/media/nvidia/Data /mnt/xavier fuse defaults,allow_other,nonempty 0 0`

----------

## NRU2

- IP address: 10.5.4.55
- MAC address: 78:d0:04:2d:b6:8e
- User: nvidia
- Password: nvidia
- Mount (fstab): `sshfs#nvidia@10.5.4.55:/media/nvidia/Data /mnt/xavier2 fuse defaults,allow_other,nonempty 0 0`

----------
----------

# Servers

## NFS server

- IP address: 10.5.3.123
- Path: /mnt/zvol1
- Mount command (Windows): `mount -o mtype=hard 10.5.3.123:/mnt/zvol1 *`
- Mount (fstab): `10.5.3.123:/mnt/zvol1 /mnt/zvol1 nfs`

# Avatar

## Cognata Simulator

- user: vayav19
- password: VVS2019

## Cognata Machine

- Anydesk: 733248617
- password: Avatar-Cognata

## Vayadrive Machine

- Anydesk: 797070277
- password: Avatar-VD

- The user password is 1 for both computers.

- Ros2 SDK for cognata receiver is at : /home/avatar-vd/VayaVision/ros2_ext_ws

- Sensor subscriber is ready on branch VV-426 and will be merged soon.

- Calibration is located on ~/Avatar/Calibration.

- The calibration generator python script is located on ~/Avatar/CreateCalib

- Steps:
  1. In Cognata Desktop, Run the Simulation sequence in the simulator
  2. In Vayadrive Desktop, Run vayadrive and use calibration located at ~/Avatar/Calibration
  3. In Vayadrive Desktop, Run cognata ros sdk located at: /home/avatar-vd/VayaVision/ros2_ext_ws; run as:  ros2 run cognata_sdk_ros2 ROS2SDK 10.8.4.103 3056 --ai
