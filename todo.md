# TO DO

[x] Presentation

[x] Upgrade JetPack

[x] Test <https://vayavision.atlassian.net/browse/VVDI-565>

[x] [TensorRT for Xavier](tensorrt.md)

[x] Test ONNX branch

Enabling DLA fails with:

``` lang-txt
TRTBase   INFO1   - parseOnnxModel: Network was set to run on DLA
13:18:31.251 [Pipe-0_LRCF360-547024400304-0x7f5d31efb0]           TRTBase   INFO1   - parseOnnxModel: Number of DLA cores: 2, set to DLA core: 0
13:18:31.253 [Pipe-0_LRCF360-547024400304-0x7f5d31efb0]           TRTBase   WARNING - log: Default DLA is enabled but layer Transpose_0 is not supported on DLA, falling back to GPU.
13:18:31.253 [Pipe-0_LRCF360-547024400304-0x7f5d31efb0]           TRTBase   WARNING - log: Default DLA is enabled but layer (Unnamed Layer* 1) [Constant] is not supported on DLA, falling back to GPU.
13:18:31.253 [Pipe-0_LRCF360-547024400304-0x7f5d31efb0]           TRTBase   WARNING - log: Default DLA is enabled but layer (Unnamed Layer* 2) [Constant] is not supported on DLA, falling back to GPU.
13:18:31.253 [Pipe-0_LRCF360-547024400304-0x7f5d31efb0]           TRTBase   WARNING - log: DLA only supports FP16 and Int8 precision type. Switching QuantizeLinear_3_quantize_scale_node device type to GPU.
13:18:31.253 [Pipe-0_LRCF360-547024400304-0x7f5d31efb0]           TRTBase   WARNING - log: Default DLA is enabled but layer (Unnamed Layer* 4) [Constant] is not supported on DLA, falling back to GPU.
13:18:31.254 [Pipe-0_LRCF360-547024400304-0x7f5d31efb0]           TRTBase   WARNING - log: Default DLA is enabled but layer (Unnamed Layer* 5) [Constant] is not supported on DLA, falling back to GPU.
13:18:31.254 [Pipe-0_LRCF360-547024400304-0x7f5d31efb0]           TRTBase   WARNING - log: Default DLA is enabled but layer DequantizeLinear_6_dequantize_scale_node is not supported on DLA, falling back to GPU.
13:18:31.254 [Pipe-0_LRCF360-547024400304-0x7f5d31efb0]           TRTBase   WARNING - log: Default DLA is enabled but layer features_0_0.weight is not supported on DLA, falling back to GPU.
13:18:31.272 [Pipe-0_LRCF360-547024400304-0x7f5d31efb0]           TRTBase   WARNING - log: Default DLA is enabled but layer Slice_858 is not supported on DLA, falling back to GPU. X 979
13:18:31.399 [Pipe-0_LRCF360-547024400304-0x7f5d31efb0]           TRTBase   ERROR   - log: 2: [dlaNode.cpp::validateGraphNode::595] Error Code 2: Internal Error (Assertion node->inputs.size() == 1 failed.)
Segmentation fault (core dumped)
```

[ ] Debug crash

----------

[ ] Debug errror in engine build.

----------
[ ] TensorRT with DLA

----------
[ ] LRCF360 Step 4: PCL to grid

----------
[ ] VDualMat with optional shared mem

----------
[x] Ask NRU about JetPack 5.0 Roadmap

----------
[x] Profile VV-959-optimize-other-portion-of-lrcf-pi  

----------
[x] VayaDrive develop with TRT7 crashes on Xavier.

- Commit 2eb04f823 introduced the bug, due to enabling logging past warning level.
- The buffer at VayaLogger.cpp@342 is too small for some messages.
- Create Jira issue (<https://vayavision.atlassian.net/browse/VV-1483>)

----------
[x] Break lrcf_buildFeaturesKernel kernel at StandaloneAlgos/LRCF360/LRCF360_cu.cu @ 307 into multiple kernels.

- Test rearranging memory.
- Test RGB features in different stream.
- Tests run on d9165273f
- Step 4.1 baseline - 15.78
- Step 4.1 shared memory - 17.85
- Step 4.1 shared + idx++ - 12.66
- Step 4.1 idx++ - 10.75
- Step 4.1 separate XYZ/RGB - 16.15
- Step 4.1 separate streams XYZ/RGB
- Step 4.1 shared memory on RGB features - 17.5
- Step 4.1 shared memory on RGB features without atomicMax - 11.56 but not BA

----------
[x] LRCF Step 4 presentation

----------
[x] Report on TensorRT 8.2 on Xavier

----------
[x] Remove TRT7 references.

----------
[x] Do not set s6_DetNetOut to default value.

![./configure](Memset_on.png)

280.79 ms/f

![./configure](Memset_off.png)

281.08 ms/f

----------
[ ] Prefetch results from Step5 earlier, using unified memory and hints.

----------
[x] Test creating continuous matrices using setGPUContinuous.

Feature matrices are already continuous. See StandaloneAlgos/LRCF360/LRCF360_Impl.cpp @ 904.

```C++
s4_rgbFeat.setGPUContinuous(true);
s4_xyzFeat.setGPUContinuous(true);
```

----------
[x] Prefetch early the inputs to Step6 (radar features).

After step 5:

```C++
m_stateForInternalThread->s5_radarFeatures.getGpuConst(m_stream);
```

287.31 ms/f vs 280.13 ms/f

----------
[x] Remove x*x and y*y.

14.61 ms vs 15.92 ms for Step 4.1

----------
[x] Remove all DepthNetProducts references in RunAlgo

[x] Combine Marko's Step3

[x] Save Markdown tables.

[x] Split buildFeatures

[x] Test SemanticNet on DLA.

[x] GetInputs: remove debug data, use vector references.

[x] Check if we can query TRT for transpose layer.

- Use <https://docs.nvidia.com/deeplearning/tensorrt/api/c_api/classnvinfer1_1_1_i_engine_inspector.html#ac8ec537097cb8c1b1af98c4094b9e70b>

[x] Separate memory code in diff branch.

[x] Remove Marko's code.

[ ] Try float half precision in Step 4.

[x] Cleanup.

[x] Use param instead of function.

[ ] Run jetson_benchmarks.

[ ] Can we split DetNet?

[x] Native compilation:

- void LanePerceptionImpl::Step2_Fit2D_CPU
- void LanePerceptionImpl::Step7_2_GetMArkerUpsample3dPoints_CPU
- void LanePerceptionImpl::Step7_1_FindEdges_CPU
- void LanePerceptionImpl::Step2_ClipGrades_CPU
- void LanePerceptionImpl::Step2_UpdateGrades_CPU
- void LanePerceptionImpl::Step_2_checkCurvature
- void LanePerceptionImpl::Step_2_checkCurvature
- void LanePerceptionImpl::Step2_FilterOutliers_CPU

[x] Test branch:

- BA test on develop w h5 files

  - not BA: Step4

- BA test on develop w onnx files

  - not BA: Step1. Cause: cv::cuda::resize (<https://github.com/opencv/opencv/issues/4728>)

  - crash: Step4 - solved. Now BA

  missing:
  
 ``` C++
 s2_RgbFeatures.copyTo(other->s2_RgbFeatures);
 s2_Error.copyTo(other->s2_Error);
 ```

[x] Fix log error message LOG_ERROR("Cannot create mapped memory for s5_radarFeatures");

[x] Add return to upload/download.

[x] Profile Marko's update  - VV-959 (4c3b7c4 vs 72d7371)

[x] Profile int8 + fp16.

[x] Check streamOverride is not null.

[x] Add "operation not supported".

[x]  Move containsTranspose function to DetNet

[x] Profiling modes 3, 13, 16 (both version) after VV-975 is merged.
