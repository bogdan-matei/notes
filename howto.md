# How To

## Git status (including igored and untracked files)

``` bash
git status --ignored --untracked
```

## Git clean (all except override and changed files)

``` bash
git clean -Xdf --exclude=\!override.*
```

## CUDA Profiling: collecting cuda traces

``` bash sudo /media/nvidia/Data/target-linux-armv8/nsys profile --trace cuda,nvtx,osrt --cudabacktrace all --sample cpu Release/VayaDriveConsole ```

## Update CMake using pip

``` bash
sudo apt remove cmake 
```

```bash
sudo pip3 install cmake --upgrade ``` or ``` sudo pip3 install cmake==3.21 
```

```bash
sudo ln -s /usr/local/bin/cmake /usr/bin/cmake 
```

```bash
cmake --version
```

## Update CMake using sources

``` bash
sudo apt remove cmake 
```

```bash
wget https://github.com/Kitware/CMake/releases/download/v3.22.2/cmake-3.22.2.tar.gz
```

```bash
tar xf cmake-3.22.2.tar.gz
```

```bash
cd cmake-3.22.2
```

```bash
sudo apt-get install libssl-dev
```

```bash
./configure
```

```bash
make
```

```bash
sudo make install
```

## Remote debugging on Xavier

- On Xavier:

```bash
gdbserver :9999 Debug/VayaDriveConsole 
```

- Add to .vscode/launch.json

``` JSON
{ 
    // on remote: gdbserver :9999 Debug/VayaDriveConsole
    "name": "(gdb) remote debugging",
    "type": "cppdbg",
    "request": "launch",
    "program": "${workspaceFolder}/Debug/VayaDriveConsole",
    "args": [],
    "stopAtEntry": false,
    "cwd": "${workspaceFolder}",
    "environment": [],
    "externalConsole": false,
    "MIMode": "gdb",
    "targetArchitecture": "arm64",
    "setupCommands": [
        {
            "description": "Enable pretty-printing for gdb",
            "text": "-enable-pretty-printing",
            "ignoreFailures": true
        }
    ],
    "miDebuggerPath": "/usr/bin/gdb-multiarch",
    "miDebuggerServerAddress": "10.5.4.55:9999"
},

```

- Launch (gdb) remote debugging on the Run and debug section in VSCode.

## Allow Docker container to run GUI apps

Run before starting container:

``` bash
xhost local:root
```

## Update Python packages on Windows

``` bash
pip freeze | %{$_.split('==')[0]} | %{pip install --user --upgrade $_}
```

## Allow Git in Docker using VSCode

``` bash
git config --global --add safe.directory /VayaVision/VayaDriveGit/vayaalgo
```

## Visual Studio Code - remove workspace cache

``` bash
rm -r ~/.config/Code/User/workspaceStorage/*
```

## Profile C++ on Linux

- Install kcachegrind

```bash
sudo apt install kcachegrind
```

- From one console:

``` bash
valgrind --tool=callgrind --dump-instr=yes -v --instr-atstart=no build_Release/Release/VayaDrive
```

- from another console:

``` bash
# start profiling
callgrind_control -i on

# dump data
callgrind_control -d

# stop profiling
callgrind_control -k

```

- Display profiling data

``` bash
kcachegrind [filename]
```

## Allow Git inside Docker

``` bash
sudo chown root:$USER ~/.ssh/config
sudo chmod 644 ~/.ssh/config
```
