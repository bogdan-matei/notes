# Comparison

## Xavier

### Base

``` bash
06/28/2022-09:50:47] [I] === Performance summary ===
[06/28/2022-09:50:47] [I] Throughput: 11.4556 qps
[06/28/2022-09:50:47] [I] Latency: min = 86.4107 ms, max = 88.0719 ms, mean = 87.2842 ms, median = 87.2968 ms, percentile(99%) = 88.0719 ms
[06/28/2022-09:50:47] [I] End-to-End Host Latency: min = 86.4169 ms, max = 88.0767 ms, mean = 87.2936 ms, median = 87.3012 ms, percentile(99%) = 88.0767 ms
[06/28/2022-09:50:47] [I] Enqueue Time: min = 1.30957 ms, max = 2.36673 ms, mean = 1.82607 ms, median = 1.76476 ms, percentile(99%) = 2.36673 ms
[06/28/2022-09:50:47] [I] H2D Latency: min = 0.126465 ms, max = 0.13623 ms, mean = 0.131208 ms, median = 0.13092 ms, percentile(99%) = 0.13623 ms
[06/28/2022-09:50:47] [I] GPU Compute Time: min = 85.6674 ms, max = 87.3192 ms, mean = 86.5353 ms, median = 86.5519 ms, percentile(99%) = 87.3192 ms
[06/28/2022-09:50:47] [I] D2H Latency: min = 0.546875 ms, max = 0.63623 ms, mean = 0.617748 ms, median = 0.619629 ms, percentile(99%) = 0.63623 ms
[06/28/2022-09:50:47] [I] Total Host Walltime: 3.14257 s
[06/28/2022-09:50:47] [I] Total GPU Compute Time: 3.11527 s
[06/28/2022-09:50:47] [I] Explanations of the performance metrics are printed in the verbose logs.
[06/28/2022-09:50:47] [I] 
&&&& PASSED TensorRT.trtexec [TensorRT v8001] # /usr/src/tensorrt/bin/trtexec --loadInputs=cropped_0000000000.png --onnx=RCF360_for_conti_no_gather_half_size_fixed_slice_twelve_one_channel_output_no_transpose.onnx
```

### FP16

``` bash
[06/28/2022-09:26:49] [I] === Performance summary ===
[06/28/2022-09:26:49] [I] Throughput: 22.1905 qps
[06/28/2022-09:26:49] [I] Latency: min = 44.2119 ms, max = 45.7253 ms, mean = 45.0549 ms, median = 45.0801 ms, percentile(99%) = 45.7253 ms
[06/28/2022-09:26:49] [I] End-to-End Host Latency: min = 44.2219 ms, max = 45.739 ms, mean = 45.0643 ms, median = 45.091 ms, percentile(99%) = 45.739 ms
[06/28/2022-09:26:49] [I] Enqueue Time: min = 1.89966 ms, max = 3.2229 ms, mean = 2.42088 ms, median = 2.29718 ms, percentile(99%) = 3.2229 ms
[06/28/2022-09:26:49] [I] H2D Latency: min = 0.125977 ms, max = 0.136719 ms, mean = 0.130569 ms, median = 0.130219 ms, percentile(99%) = 0.136719 ms
[06/28/2022-09:26:49] [I] GPU Compute Time: min = 43.47 ms, max = 44.9623 ms, mean = 44.3067 ms, median = 44.3409 ms, percentile(99%) = 44.9623 ms
[06/28/2022-09:26:49] [I] D2H Latency: min = 0.5354 ms, max = 0.632446 ms, mean = 0.617619 ms, median = 0.618835 ms, percentile(99%) = 0.632446 ms
[06/28/2022-09:26:49] [I] Total Host Walltime: 3.06438 s
[06/28/2022-09:26:49] [I] Total GPU Compute Time: 3.01286 s
[06/28/2022-09:26:49] [I] Explanations of the performance metrics are printed in the verbose logs.
[06/28/2022-09:26:49] [I] 
&&&& PASSED TensorRT.trtexec [TensorRT v8001] # /usr/src/tensorrt/bin/trtexec --loadInputs=cropped_0000000000.png --onnx=RCF360_for_conti_no_gather_half_size_fixed_slice_twelve_one_channel_output_no_transpose.onnx --fp16
```

### int8

``` bash
[06/28/2022-11:36:20] [I] === Performance summary ===
[06/28/2022-11:36:20] [I] Throughput: 31.4285 qps
[06/28/2022-11:36:20] [I] Latency: min = 31.3625 ms, max = 32.1982 ms, mean = 31.8089 ms, median = 31.8342 ms, percentile(99%) = 32.1982 ms
[06/28/2022-11:36:20] [I] End-to-End Host Latency: min = 31.3726 ms, max = 32.2087 ms, mean = 31.8182 ms, median = 31.8481 ms, percentile(99%) = 32.2087 ms
[06/28/2022-11:36:20] [I] Enqueue Time: min = 1.39954 ms, max = 1.65497 ms, mean = 1.47652 ms, median = 1.4733 ms, percentile(99%) = 1.65497 ms
[06/28/2022-11:36:20] [I] H2D Latency: min = 0.125732 ms, max = 0.135742 ms, mean = 0.129796 ms, median = 0.129639 ms, percentile(99%) = 0.135742 ms
[06/28/2022-11:36:20] [I] GPU Compute Time: min = 30.6266 ms, max = 31.4419 ms, mean = 31.0619 ms, median = 31.0864 ms, percentile(99%) = 31.4419 ms
[06/28/2022-11:36:20] [I] D2H Latency: min = 0.541992 ms, max = 0.630859 ms, mean = 0.617161 ms, median = 0.617798 ms, percentile(99%) = 0.630859 ms
[06/28/2022-11:36:20] [I] Total Host Walltime: 3.05455 s
[06/28/2022-11:36:20] [I] Total GPU Compute Time: 2.98194 s
[06/28/2022-11:36:20] [I] Explanations of the performance metrics are printed in the verbose logs.
[06/28/2022-11:36:20] [I] 
&&&& PASSED TensorRT.trtexec [TensorRT v8001] # /usr/src/tensorrt/bin/trtexec --loadInputs=cropped_0000000000.png --onnx=RCF360_for_conti_no_gather_half_size_fixed_slice_twelve_one_channel_output_no_transpose.onnx --fp16 --int8
```
