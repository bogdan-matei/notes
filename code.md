# Code samples

## Export OpenCV matrices to file

``` C++

    static int index = 0;
    std::string fileName = "s4_xyzFeat_" + std::to_string(index) + "_gpu.bmp";
    cv::Mat1f mat;
    tmp.s4_xyzFeat.getGpu(m_stream).download(mat, *m_stream);
    m_stream->waitForCompletion();
    cv::imwrite(fileName, mat);

    fileName = "s4_xyzFeat_" + std::to_string(index++) + "_cpu.bmp";
    cv::imwrite(fileName, state.s4_xyzFeat.getCpu(m_stream));

    std::string fileName = "s4_xyzFeat_" + std::to_string(index) + "_gpu.yml";
    cv::FileStorage fs1(fileName, cv::FileStorage::WRITE);
    fs1 << "matrix" << mat;
    fs1.release();

    fileName = "s4_xyzFeat_" + std::to_string(index++) + "_cpu.yml";
    cv::FileStorage fs2(fileName, cv::FileStorage::WRITE);
    fs2 << "matrix" << state.s4_xyzFeat.getCpu(m_stream);
    fs2.release();
```

## Profile CUDA

``` Bash
    nsys profile --trace cuda,nvtx,osrt  --cudabacktrace all [application]
```

## Verify Step 4 results

``` C++
    cv::Mat1f cRGBFeatures;
    RGBFeatures.download(cRGBFeatures, *m_stream);
    m_stream->waitForCompletion();
    Mat1f rgbFeat(BEVRows * 10, BEVCols, CV_32FC1);
    rgbFeat.setTo(MIN_VAL);
    for (int r = 0; r < BEVRows; ++r)
    {
        for (int c = 0; c < BEVCols; ++c)
        {
            int idx = r;
            for (int i = 0; i < 10; ++i)
            {
                rgbFeat(idx, c) = cRGBFeatures(r, c * 10 + i);
                idx += BEVRows;
            }
        }
    }
    cv::imwrite("rgbFeat.bmp", rgbFeat);
    cv::Mat1f cXYZFeatures;
    XYZFeatures.download(cXYZFeatures, *m_stream);
    m_stream->waitForCompletion();
    Mat1f xyzFeat(BEVRows * 9, BEVCols, CV_32FC1);
    xyzFeat.setTo(MIN_VAL);
    for (int r = 0; r < BEVRows; ++r)
    {
        for (int c = 0; c < BEVCols; ++c)
        {
            int idx = r;
            for (int i = 0; i < 9; ++i)
            {
                xyzFeat(idx, c) = cXYZFeatures(r, c * 9 + i);
                idx += BEVRows;
            }
        }
    }
    cv::imwrite("xyzFeat.bmp", xyzFeat);
    // cv::FileStorage fs1("cRGBFeatures.xml", cv::FileStorage::WRITE);
    // fs1 << "cRGBFeatures" << cRGBFeatures;
    // fs1.release();
    cv::imwrite("cRGBFeatures.bmp", cRGBFeatures);
    // cv::FileStorage fs2("cXYZFeatures.xml", cv::FileStorage::WRITE);
    // fs2 << "cXYZFeatures" << cXYZFeatures;
    // fs2.release();
    cv::imwrite("cXYZFeatures.bmp", cXYZFeatures);
```
